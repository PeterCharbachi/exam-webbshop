import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageViewComponent } from './main-page/main-page-view/main-page-view.component';
import { MainPageModule } from './main-page/main-page.module';

const appRoutes: Routes = [
  { path: '', component: MainPageViewComponent },
  { path: '**', redirectTo: '/main-page', pathMatch: 'full' },

];

@NgModule({
  imports: [
    MainPageModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
