import { Component } from '@angular/core';
import { ICategory, ISuppliers, IProductView, ICustomer } from './main-page/mainPage';
import { CategoryService } from './shared/services/categories/categories.service';
import { CompanyService } from './shared/services/companies/companies.service';
import { ProductsService } from './shared/services/products/products.service';
import { Router } from '@angular/router';
import { LoginService } from './shared/services/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'examensarbete';
  categories: ICategory[] = [];
  companies: ISuppliers[] = [];
  products: IProductView = {} as IProductView;
  newProduct: IProductView[] = [];
  user: ICustomer = {} as ICustomer;

  constructor(private categoriesApi: CategoryService, private companiesApis: CompanyService, private productsApi: ProductsService, public router: Router, private loginApi: LoginService, private mainPageApi: ProductsService, ) { }

  ngOnInit() {
    this.mainPageApi.getProductsBySuppliers().subscribe(
      data => {
        this.products = data;
    })
    
    this.categoriesApi.getCategories().subscribe(
      data => {
        this.categories = data;
    })

    this.companiesApis.getCaompanies().subscribe(
      data => {
        this.companies = data;
    })

    let secret = JSON.parse(sessionStorage.getItem('secret'));
    secret.secret
    this.loginApi.isLoggedIn(secret.secret).subscribe(
      data => {
        this.user = data;
        console.log(this.user);
    })
  }

  // söker efter produkt och om den finns routar man till produkten sida
  productSearch(title:string) {
    
    const name = title;
    this.productsApi.getProductsBySearch(name).subscribe(
      data => {
        this.products = data;
        console.log(this.products[0].product_id);
        // this.newProduct.push(this.products);
       this.router.navigate(['/products/'+ this.products[0].product_id]);
    });
  
    
  }
  

  getNameOfLoggedInUser(){

    let data = JSON.parse(sessionStorage.getItem('secret'))
    if(data === null) 
      return null;
    return data.user;
  }

}
