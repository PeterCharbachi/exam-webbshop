import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { ISuppliers, IImageBar, INavbar, IHeaderImage } from 'src/app/main-page/mainPage';



@Injectable({
    providedIn: 'root'
})
export class CompanyService {
    constructor(private http: HttpClient) { }
    data: ISuppliers;
    host:string = 'http://localhost';
    getCaompanies(): Observable<ISuppliers[]> {
        const url = 'http://localhost/project/src/public/api/suppliers';
       return this.http.get<ISuppliers[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }
    getCompaniesById(id): Observable<ISuppliers[]> {
        const url = 'http://localhost/project/src/public/api/suppliers/' + id;
       return this.http.get<ISuppliers[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }

    getNavbarBySupplier(id): Observable<INavbar[]> {
      const url = this.host + '/project/src/public/api/supplier/homepage/navbar/' + id;
     return this.http.get<INavbar[]>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }
  
  getImageHeaderBySupplier(id): Observable<IHeaderImage> {
    const url = this.host + '/project/src/public/api/supplier/homepage/imgHeader/' + id;
   return this.http.get<IHeaderImage>(url).pipe(
    tap( // Log the result or error
    //   data => this.data = data
    data =>  console.log(data),
    )
  );
}

    getImageBarBySupplier(id): Observable<IImageBar[]> {
      const url = this.host + '/project/src/public/api/supplier/homepage/imgBar/' + id;
     return this.http.get<IImageBar[]>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

     
}