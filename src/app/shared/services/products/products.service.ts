import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";
import { IProductView, IReview, IOrder } from 'src/app/main-page/mainPage';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  
  constructor(private http: HttpClient) { }
  data: IProductView;

  getProductsBySuppliers(): Observable<IProductView> {
    const url = 'http://localhost/project/src/public/api/products';
   return this.http.get<IProductView>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: IProductView) => {
      return data;
    }));
  }

  getProductsById(id): Observable<IProductView> {
    const url = 'http://localhost/project/src/public/api/products/' + id;
   return this.http.get<IProductView>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: IProductView) => {
      return data;
    }));
  }

  getProductsByCategories(id): Observable<IProductView[]> {
    const url = 'http://localhost/project/src/public/api/categories/'+ id +'/products';
   return this.http.get<IProductView[]>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: IProductView[]) => {
      return data;
    }));
  }

  getProductsByStoreCategories(id): Observable<IProductView[]> {
    const url = 'http://localhost/project/src/public/api/store/categories/'+ id +'/products';
   return this.http.get<IProductView[]>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: IProductView[]) => {
      return data;
    }));
  }

  getProductsBySearch(name: string): Observable<IProductView> {
    const url = 'http://localhost/project/src/public/api/products/search/'+ name ;
   return this.http.get<IProductView>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: IProductView) => {
      return data;
    }));
  }


  postReviews(reviews: IReview): Observable<any> {
    const url = 'http://localhost/project/src/public/api/customer/reviews/add';
    return this.http.post<any>(url, reviews).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: any) => {
      return data;
    }));
  }

  getReviewsByProductId(pid): Observable<IReview[]> {
    const url = 'http://localhost/project/src/public/api/customer/reviews/product/' + pid;
   return this.http.get<IReview[]>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: IReview[]) => {
      return data;
    }));
  }

  postOrder(product: IOrder): Observable<any> {
    const url = 'http://localhost/project/src/public/api/orders/add';
    return this.http.post<any>(url, product).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: any) => {
      return data;
    }));
  }

}
