import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { ICategory } from 'src/app/main-page/mainPage';



@Injectable({
    providedIn: 'root'
})
export class CategoryService {
    constructor(private http: HttpClient) { }
    data: ICategory;

    getCategories(): Observable<ICategory[]> {
        const url = 'http://localhost/project/src/public/api/categories';
       return this.http.get<ICategory[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }
    getCategoriesById(id): Observable<ICategory[]> {
        const url = 'http://localhost/project/src/public/api/supplier/categories/' + id;
       return this.http.get<ICategory[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }

     
}