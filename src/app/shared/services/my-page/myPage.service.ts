import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class MyPageService {

  constructor(private http: HttpClient) { }
  host:string = 'http://localhost';

  putUser(user): Observable < any > {
    return this.http.put < any > (this.host + '/project/src/public/api/customer/put', user).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }
}