import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from 'src/app/shared/services/categories/categories.service';
import { ICategory } from '../mainPage';

@Component({
  selector: 'app-main-page-categories',
  templateUrl: './main-page-categories.component.html',
  styleUrls: ['./main-page-categories.component.css']
})
export class MainPageCategoriesComponent implements OnInit {
  category: ICategory[] = [];

  constructor(private root: ActivatedRoute, private mainPageApi: CategoryService) { }

  ngOnInit() {
    // const id = this.root.snapshot.paramMap.get('id');
    // this.mainPageApi.getCategoriesById(id).subscribe(
    //   data => {
    //     this.category = data;
    // })

    this.root.params.subscribe(
      params => {
          const id = +params['id'];
          this.mainPageApi.getCategoriesById(id).subscribe(
            data => {
              this.category = data;
          })
      }
    );
  }
}
