import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageCategoriesComponent } from './main-page-categories.component';

describe('MainPageCategoriesComponent', () => {
  let component: MainPageCategoriesComponent;
  let fixture: ComponentFixture<MainPageCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPageCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
