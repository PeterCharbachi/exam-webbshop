import { Component, OnInit } from '@angular/core';
import { Ilogin, ISignUp } from '../mainPage';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  tabIndex: number = 1;
  cred: Ilogin = {} as Ilogin;
  signUp: ISignUp = {} as ISignUp;

  messageForm: FormGroup;
  submitted = false;
  success: boolean;
  username: string;
  password: string;

  constructor(private loginApi: LoginService, private location: Location, private _router: Router, private formBuilder: FormBuilder) {
    this.messageForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
   }

  ngOnInit() {
  }

  // signup funktion
  onSignUp() {

    this.loginApi.createLogin(this.signUp).subscribe(
      (data => {
        this.tabIndex = 1
      })
    )

  }

  // loggin funktion
  login(){
    this.username = this.cred.username;
    this.password = this.cred.password;

    this.submitted = true;
    if (this.messageForm.invalid) {
      return this.success = false;
    }

    if (this.messageForm.valid) {
      
      this.loginApi.postLogin(this.cred).subscribe(
        data => {
          sessionStorage.setItem('secret', JSON.stringify(data));
          if(data.secret != null) {
            this._router.navigateByUrl('/mypage');
          }
          if (data == false){
            return this.success = false;
          }else {
            return this.success = true;
          }
      })
      return this.success = true;
    }

  }

}
