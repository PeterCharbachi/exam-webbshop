import { Component, OnInit } from '@angular/core';
import { ICustomer } from '../mainPage';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { MyPageService } from 'src/app/shared/services/my-page/myPage.service';

@Component({
  selector: 'app-main-page-mypage',
  templateUrl: './main-page-mypage.component.html',
  styleUrls: ['./main-page-mypage.component.css']
})
export class MainPageMypageComponent implements OnInit {
  user: ICustomer = {} as ICustomer;
  constructor(private loginApi: LoginService, private customerApi: MyPageService ) { }

  ngOnInit() {
    // hämtar secret från sessionstorage och kollar om usern är inloggad.
    let secret = JSON.parse(sessionStorage.getItem('secret'));
    secret.secret
    this.loginApi.isLoggedIn(secret.secret).subscribe(
      data => {
        this.user = data;
        console.log(this.user);
    })
  }

  getNameOfLoggedInUser(){

    let data = JSON.parse(sessionStorage.getItem('secret'));
    if(data === null) 
      return null;
    return data.user;
  }

  // uppdaterar sina uppgifter
  updateUser(){
    let cid = JSON.parse(sessionStorage.getItem('secret'));
    cid = cid.user
    this.user.customer_id = cid;
    this.customerApi.putUser(this.user).subscribe(
      (data => { 
        location.reload();
      })
    )
    
  }
}
