export interface ISuppliers {
    supplier_id: number;
    company_name: string;
    contact_first_name: string;
    contact_last_name: string;
    address1: string;
}
export interface ICustomer {
    customer_id: number;
    first_name: string;
    last_name: string;
    address1: string;
    address2: string;
    phone: number;
    email: string;
}

export class IProductView {
    product_id: number;
    sku: string;
    product_name: string;
    product_description: string;
    supplier_id: number;
    category_id: number;
    price: number;
    picture: string;
    count: number;
    sum:number;

}

export interface ICategory {
    category_id: number;
    category_name: string;
    description: string;
    active: boolean;
    supplier_id: number;
}


export interface IImageBar{
    supplierId: number;
    homepageIB_name: number;
    homepageIB_id: string;

}
export interface IHomePageAboutUs{
    supplier_id: number;
    address: number;
    zipCode: number;
    city: string;
    number: number;
    email: string;
    textField: string;
}

export interface INavbar{
    homePageN_name: string;
    supplier_id: number;
    category_id: number;
    description: string;
    navbarId: string;
}


export interface IHeaderImage{
    homePageIB_id: number;
    homePageIB_sid: number;
    homePageIB_Image: string;
}

export interface IReview {
    customer_id: number;
    product_id: number;
    reviewsText: string;
}

export class IOrder {
    customer_id: number;
    first_name: string;
    last_name: string;
    address1: string;
    address2: string;
    phone: number;
    email: string;
    order_sum:any
    
    products: IProductView[] = [];
}

export interface Ilogin{
    username: string;
    password: string;
}

export interface ISignUp {
    firstName: string;
    lastName: string;
    address1: string;
    address2: string;
    phone: number;
    email: string;
    password: string;
}