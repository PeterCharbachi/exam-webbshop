import { Component, OnInit } from '@angular/core';
import { IProductView, IOrder, ICustomer } from '../mainPage';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { LoginService } from 'src/app/shared/services/login/login.service';


@Component({
  selector: 'app-main-page-checkout',
  templateUrl: './main-page-checkout.component.html',
  styleUrls: ['./main-page-checkout.component.css']
})
export class MainPageCheckoutComponent implements OnInit {
  user: ICustomer = {} as ICustomer;
  products: IProductView = {} as IProductView;
  newProduct: IProductView[] = []
  total:any = '';
  
  constructor(private mainPageApi: ProductsService, private loginApi: LoginService) { }

  ngOnInit() {
    // hämtar secret från sessionstorage och kollar om usern är inloggad.
    let secret = JSON.parse(sessionStorage.getItem('secret'));
    secret.secret
    this.loginApi.isLoggedIn(secret.secret).subscribe(
      data => {
        this.user = data;
        console.log(this.user);
    })


    let order = localStorage.getItem('order');
    let orderItemList = JSON.parse(order);
    for (let i = 0; i < orderItemList.length; i++) {
     
      let element = orderItemList[i].product_id;
      let count = orderItemList[i].count;
      let id = element;
      this.mainPageApi.getProductsById(id).subscribe(
        data => {
          this.products = data;
          this.newProduct.push(this.products);
          this.products.count = Number(count);
          let sum = []
          for (let i = 0; i < this.newProduct.length; i++) {
            const element = this.newProduct[i];
            let total = element.price * element.count;
            sum.push(+total);
          }
          sum = sum.reduce(function(a, b) { return a + b; }, 0);
          this.total = Number(sum);
      })  
    }
    console.log(this.newProduct);
      
    

  }


  postOrder() {
    let user = JSON.parse(sessionStorage.getItem('secret'));
    const sid = user.user;
    const firstName = this.user.first_name;
    const lastName = this.user.last_name;
    const phoneNumber = this.user.phone;
    const email = this.user.email;
    const address = this.user.address1;


    let checkOut = new IOrder();
    checkOut.customer_id = sid;
    checkOut.first_name = firstName;
    checkOut.last_name = lastName;
    checkOut.phone = phoneNumber;
    checkOut.email = email;
    checkOut.address1 = address;
    checkOut.order_sum = this.total;

    for (let i = 0; i < this.newProduct.length; i++) {

      checkOut.products.push(this.newProduct[i]);
    }
    console.log(checkOut);

    this.mainPageApi.postOrder(checkOut).subscribe(
      data => {
        localStorage.removeItem('order')
        location.reload();
      })  
  }
 
}
