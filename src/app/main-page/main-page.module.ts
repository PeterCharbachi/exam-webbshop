import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSortModule, MatRippleModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MainPageCategoriesComponent } from './main-page-categories/main-page-categories.component';
import { MainPageCompaniesComponent } from './main-page-companies/main-page-companies.component';
import { MainPageProductsComponent } from './main-page-products/main-page-products.component';
import { MainPageCheckoutComponent } from './main-page-checkout/main-page-checkout.component';
import { AllProductsComponent } from './main-page-products/all-products/all-products.component';
import { SingelProductComponent } from './main-page-products/singel-product/singel-product.component';
import { LoginComponent } from './login/login.component';
import { MainPageMypageComponent } from './main-page-mypage/main-page-mypage.component';


const appRoutes: Routes = [
  { path: "category", component: MainPageCategoriesComponent },
  { path: "category/:id", component: MainPageCategoriesComponent },
  { path: "login", component: LoginComponent },
  { path: "mypage", component: MainPageMypageComponent },
  { path: "products", component: MainPageProductsComponent, children: [
    { path: ":id", component: SingelProductComponent },
    { path: "store/:id", component: SingelProductComponent },
    { path: "category/:id", component: AllProductsComponent },
    { path: "store/category/:id", component: AllProductsComponent },
  ] },
  
  // { path: "store", component: MainPageCompaniesComponent },
  { path: "store/:id", component: MainPageCompaniesComponent },
  { path: "checkout", component: MainPageCheckoutComponent },
];

@NgModule({
  declarations: [
    MainPageCategoriesComponent, 
    MainPageCompaniesComponent, MainPageProductsComponent, MainPageCheckoutComponent, AllProductsComponent, SingelProductComponent, LoginComponent, MainPageMypageComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    ReactiveFormsModule,  
    [RouterModule.forRoot(appRoutes)],
  ],
  exports: [RouterModule]
})
export class MainPageModule { }
