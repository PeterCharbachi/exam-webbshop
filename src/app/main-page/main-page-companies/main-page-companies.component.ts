import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ISuppliers, INavbar, IImageBar, IHeaderImage } from '../mainPage';
import { CompanyService } from 'src/app/shared/services/companies/companies.service';

@Component({
  selector: 'app-main-page-companies',
  templateUrl: './main-page-companies.component.html',
  styleUrls: ['./main-page-companies.component.css']
})
export class MainPageCompaniesComponent implements OnInit {
  navbar: INavbar[] = []
  imgHeader: IHeaderImage = {} as IHeaderImage;
  imgBar: IImageBar[] = [];
  companies: ISuppliers[] = []
  constructor(private root: ActivatedRoute, private companiesApi: CompanyService, private _router: Router) {
    // hämtar store när man byter store på sidan
    this._router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
    }
   }

  ngOnInit() {
    // hämtar allt som är sparat från databasen när man skapade det i admin läget 
    const id = this.root.snapshot.paramMap.get('id');
    this.companiesApi.getCompaniesById(id).subscribe(
      data => {
        this.companies = data;
    })


    this.companiesApi.getNavbarBySupplier(id).subscribe(
      data => {
        this.navbar = data;
    })
    this.companiesApi.getImageHeaderBySupplier(id).subscribe(
      data => {
        this.imgHeader = data;
    })
    this.companiesApi.getImageBarBySupplier(id).subscribe(
      data => {
        this.imgBar = data;
    })

    // this.root.params.subscribe(
    //   params => {
    //       const id = +params['id'];
    //       this.companiesApi.getCompaniesById(id).subscribe(
    //         data => {
    //           this.companies = data;
    //       })
    //   }
    // );
  }
  
}

