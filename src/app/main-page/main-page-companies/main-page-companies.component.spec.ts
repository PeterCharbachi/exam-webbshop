import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageCompaniesComponent } from './main-page-companies.component';

describe('MainPageCompaniesComponent', () => {
  let component: MainPageCompaniesComponent;
  let fixture: ComponentFixture<MainPageCompaniesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPageCompaniesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageCompaniesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
