import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IProductView, IReview } from '../../mainPage';
import { ProductsService } from 'src/app/shared/services/products/products.service';

@Component({
  selector: 'app-singel-product',
  templateUrl: './singel-product.component.html',
  styleUrls: ['./singel-product.component.css']
})
export class SingelProductComponent implements OnInit {
  Allproducts: IProductView = {} as IProductView;
  newProduct: IProductView[] = []
  reviews: IReview = {} as IReview;
  reviewsByProduct: IReview[] = [];

  tabIndex: number = 1;

  constructor(private root: ActivatedRoute, private mainPageApi: ProductsService) { }

  ngOnInit() {
    this.showReview();
    this.root.params.subscribe(
      params => {
          const id = +params['id'];
          this.mainPageApi.getProductsById(id).subscribe(
            data => {
              this.Allproducts = data;
              // this.newProduct.push(this.products);
          })
      }
    );
  }

  showReview() {
    const pid = this.root.snapshot.paramMap.get('id');
    this.mainPageApi.getReviewsByProductId(pid).subscribe(
      data => {
        this.reviewsByProduct = data;
    })
  }
  

  isLoggedIn() {
    let data = JSON.parse(sessionStorage.getItem('secret'));
    if(data === null) 
      return null;
    return data.secret = true;
  }


  saveReview(){
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.reviews.customer_id = sid;
    this.reviews.product_id = this.Allproducts.product_id;
    this.mainPageApi.postReviews(this.reviews).subscribe(
      (data => { 
        this.showReview();
      })
    )
  }

  
  addToCart(data) {

    // let count:number = 1
    // let productsName = data.product_name;
    // let productsId = data.product_id;
    // var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
    
    // var newItem = {
    //   'product-name': productsId,
    //   'product-id': productsName,
    //   'count': count,

    // };
    //   oldItems.push(newItem);
    
    // localStorage.setItem('itemsArray', JSON.stringify(oldItems));
    var order = localStorage.getItem('order');
   
    if (order == null) {
      let orderItemList = [];

      let products = new IProductView();
      products.product_id = data
      products.count = 1
    
   
    orderItemList.push(products);
    localStorage.setItem('order', JSON.stringify(orderItemList));
    }
     else{

      let orderItemList = JSON.parse(order);

      //försök hitta produkten.
      var produkt = orderItemList.find(x => x.product_id == data)

      if (produkt == undefined) {
        let products = new IProductView();
        products.product_id = data;
        products.count = 1

        orderItemList.push(products);
      } else {
        //ta reda på vart i listan den är.
        var index = orderItemList.indexOf(produkt);

        produkt.count = Number(produkt.count) + 1;
        orderItemList.splice(index, 1, produkt);

      }

      localStorage.removeItem('order');
      localStorage.setItem('order', JSON.stringify(orderItemList));


    }

    }

}
