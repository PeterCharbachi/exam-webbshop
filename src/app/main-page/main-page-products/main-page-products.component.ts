import { Component, OnInit } from '@angular/core';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { ActivatedRoute } from '@angular/router';
import { IProductView, ICategory } from '../mainPage';
import { CategoryService } from 'src/app/shared/services/categories/categories.service';


@Component({
  selector: 'app-main-page-products',
  templateUrl: './main-page-products.component.html',
  styleUrls: ['./main-page-products.component.css']
})
export class MainPageProductsComponent implements OnInit {

  products: IProductView;
  categories: ICategory[] = [];
  
  constructor(private root: ActivatedRoute, private mainPageApi: ProductsService, private categoriesApi: CategoryService,) { }

  ngOnInit() {
    // hämta alla produkter
    this.mainPageApi.getProductsBySuppliers().subscribe(
      data => {
        this.products = data;
    })
    // hämtar alla kategorie
    this.categoriesApi.getCategories().subscribe(
      data => {
        this.categories = data;
    })
  }

  // togglar när man trycker på kategorier 
  toggleCategory() {
    var x = document.getElementById("mySidebar");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
  

  addToCart(data) {

    // let count:number = 1
    // let productsName = data.product_name;
    // let productsId = data.product_id;
    // var oldItems = JSON.parse(localStorage.getItem('itemsArray')) || [];
    
    // var newItem = {
    //   'product-name': productsId,
    //   'product-id': productsName,
    //   'count': count,

    // };
    //   oldItems.push(newItem);
    
    // localStorage.setItem('itemsArray', JSON.stringify(oldItems));
    var order = localStorage.getItem('order');
   
    if (order == null) {
      let orderItemList = [];

      this.products = new IProductView();
      this.products.product_id = data.product_id
      this.products.count = 1
    
   
    orderItemList.push(this.products);
    localStorage.setItem('order', JSON.stringify(orderItemList));
    } else{

      let orderItemList = JSON.parse(order);

      //försök hitta produkten.
      var produkt = orderItemList.find(x => x.product_id == data.product_id)

      if (produkt == undefined) {
        this.products = new IProductView();
        this.products.product_id = data.product_id
        this.products.count = 1

        orderItemList.push(this.products);
      } else {
        //ta reda på vart i listan den är.
        var index = orderItemList.indexOf(produkt);

        produkt.count = Number(produkt.count) + 1;
        orderItemList.splice(index, 1, produkt);

      }

      localStorage.removeItem('order');
      localStorage.setItem('order', JSON.stringify(orderItemList));


    }

  }
  

}
