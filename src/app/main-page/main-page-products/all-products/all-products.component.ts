import { Component, OnInit, ViewChild } from '@angular/core';
import { IProductView, ICategory } from '../../mainPage';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.css']
})
export class AllProductsComponent implements OnInit {

  products: IProductView[] = [];
  productsStore: IProductView[] = [];
  categories: ICategory  = {} as ICategory;;
  dataSource: MatTableDataSource<IProductView>;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(private root: ActivatedRoute, private mainPageApi: ProductsService) {
    this.dataSource = new MatTableDataSource();
   }

  ngOnInit() {

    // hämtaralla produkter från specifik kategori id kommer från parameneten
    this.root.params.subscribe(
      params => {
          const id = +params['id'];
          this.mainPageApi.getProductsByCategories(id).subscribe(
            data => {
              this.products = data;
              // this.dataSource.data = data
          })
      }
    );

    this.root.params.subscribe(
      params => {
          const id = +params['id'];
          this.mainPageApi.getProductsByStoreCategories(id).subscribe(
            data => {
              this.productsStore = data;
              // this.dataSource.data = data
          })
      }
    );
    
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



}
